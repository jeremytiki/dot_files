set fileformat=unix " Force Unix filetypes
set encoding=utf-8

au FocusLost * :wa " Auto save on focus off
let g:vim_json_syntax_conceal = 0
set conceallevel=0 "Hides json quotes for some godforsaken reason
au FileType * setl cole=0
augroup Markdown
        autocmd!
        autocmd FileType markdown set wrap
        autocmd FileType markdown set tw=80
        autocmd FileType markdownset columns=80
        autocmd FileType markdown set linebreak
        autocmd FileType markdown set showbreak=+++
augroup END


if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
  silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
endif

call plug#begin('~/.config/nvim/plugged')

" Colorschemes
Plug 'folke/twilight.nvim'
Plug 'eddyekofo94/gruvbox-flat.nvim'
Plug 'shaunsingh/moonlight.nvim'
Plug 'spolu/dwm.vim'


" Provides Fuzzy Finding & Popup TUI
Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
Plug 'glepnir/dashboard-nvim'

" NerdTree replacememnt
Plug 'kyazdani42/nvim-web-devicons' " for file icons
Plug 'kyazdani42/nvim-tree.lua'

" Tagbar
Plug 'majutsushi/tagbar'
Plug 'liuchengxu/vista.vim'

" Column Markers
" Plug '/tpope/vim-commentary'
Plug 'b3nj5m1n/kommentary'
Plug 'kshenoy/vim-signature'


," Yank help
" Plug 'tversteeg/registers.nvim', { 'branch': 'main' }
Plug 'gennaro-tedesco/nvim-peekup'
Plug 'machakann/vim-highlightedyank'

" Git
" Plug 'airblade/vim-gitgutter'
Plug 'ruifm/gitlinker.nvim'
Plug 'lewis6991/gitsigns.nvim'
Plug 'tpope/vim-fugitive'
Plug 'jreybert/vimagit'
Plug 'idanarye/vim-merginal'

" Status Line
Plug 'hoob3rt/lualine.nvim'
" Plug 'vim-airline/vim-airline'
Plug 'kyazdani42/nvim-web-devicons'

"" Python Specific
" Movement
Plug 'jeetsukumaran/vim-pythonsense'
" Indenting
Plug 'lukas-reineke/indent-blankline.nvim'
Plug 'Vimjas/vim-python-pep8-indent'

" Folding
Plug 'kalekundert/vim-coiled-snake'
Plug 'Konfekt/FastFold'

" Highlighting
Plug 'numirias/semshi', {'do': ':UpdateRemotePlugins'}
Plug 'unblevable/quick-scope'

" TreeSitter + Plugins
Plug 'nvim-treesitter/nvim-treesitter',
Plug 'lewis6991/spellsitter.nvim',

" Parens
Plug 'jiangmiao/auto-pairs'

" CoC
 Plug 'neoclide/coc.nvim', {'branch': 'release'}
 Plug 'pappasam/coc-jedi', { 'do': 'yarn install --frozen-lockfile && yarn build' }

" Icons in completion
Plug 'onsails/lspkind-nvim'

call plug#end()

colorscheme moonlight

let mapleader = ","
set number
set relativenumber
set noswapfile

" Sets the highlighted cursor line
set cursorline
hi CursorLine ctermbg=0 "8 = dark gray, 15 = white
hi Cursor ctermbg=15 ctermfg=8

" Allows <tab> to cycle through auto completions
inoremap <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"

" Search configuration
set ignorecase " ignore case when searching
set smartcase " turn on smartcase

inoremap jj <ESC>

" Easier Split resizes
map <leader>- <esc>:res -5<cr>
map <leader>= <esc>:res +5<cr>
map <leader>} <esc>:vertical resize -5<cr>
map <leader>{ <esc>:vertical resize +5<cr>

" Move down one line only
map j gj
map k gk

" Better Indentation
vnoremap < <gv
vnoremap > >gv

" Use python/perl search
nnoremap / /\v
vnoremap / /\v

set nowrap " Do not auto wrap text on load
set expandtab
" Close all other buffers with 2 spaces
noremap <leader><Space><Space> :only<cr>
noremap <leader>q :bd<cr>
""""""""""""""""""""""""""""""""""""
"
""" Git Status
"
"""""""""""""""""""""""""""""""""""
noremap <leader>gs :Gstatus<cr>
""""""""""""""""""""""""""""""""""""
"
""" Merginal
"
"""""""""""""""""""""""""""""""""""
noremap <leader>gm :MerginalToggle<cr>

" Fixes issue where auto complete window stays open even after completed
autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif

""""""""""""""""""""""""""""""""""""
"
""" Javascript Specific
"
"""""""""""""""""""""""""""""""""""

autocmd BufNewFile,BufRead *.(js|vue|yml)   set tabstop=2
autocmd BufNewFile,BufRead *.(js|vue|yml)   set shiftwidth=2
autocmd BufNewFile,BufRead *.(js|vue|yml)   set softtabstop=2

""""""""""""""""""""""
"
""  Telescope
"
""""""""""""""""""""""
nnoremap <leader>p <cmd>Telescope find_files<cr>
nnoremap <leader>f <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>; <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>

""""""""""""""""""""""
"
"" NvimTree
"
""""""""""""""""""""""
nnoremap <leader>o :NvimTreeToggle<CR>
nnoremap <leader>nr :NvimTreeRefresh<CR>
nnoremap <leader>nf :NvimTreeFindFile<CR>

""""""""""""""""""""""
"
""" vim-highlightedyank
"
""""""""""""""""""""""
"hi HighlightedyankRegion cterm=reverse gui=reverse<Paste>
let g:highlightedyank_highlight_duration = 1000

""""""""""""""""""""""
"
""  LuaLine
"
""""""""""""""""""""""
set termguicolors
lua require('lualine').setup {options = {theme = 'gruvbox-flat'}}
" lua require('bufferline').setup {options = {theme = 'sonokai'}}

"""""""""""""""""""""
"
"" Dashboard
"
"""""""""""""""""""""
let g:dashboard_default_executive ='telescope'

let g:dashboard_custom_header = [
    \'',
    \' .              +   .                .   . .     .  .     ',
    \'                   .                    .       .     *   ',
    \'  .       *                        . . . .  .   .  + .    ',
    \'            "You Are Here"            .   .  +  . . .     ',
    \'.                |             .  .   .    .    . .     ',
    \'                 |           .     .     . +.    +  .   ',
    \'                \|/            .       .   . .         ',
    \'       . .       V          .    * . . .  .  +   .        ',
    \'          +      .           .   .      +                 ',
    \'                           .       . +  .+. .             ',
    \' .                      .     . + .  . .     .            ',
    \'          .      .    .     . .   . . .       \ ! /      ',
    \'     *             .    . .  +    .  .        - O -        ',
    \'         .     .    .  +   . .  *  .       .  / | \        ',
    \'              . + .  .  .  .. +  .                        ',
    \'      .  .  .  *   .  *  . +..  .            *            ',
    \'.      .   . .   .   .   . .  +   .    .            +     ',
    \'',
    \]

"""""""""""""""""""""
"
"" lspkind-nvim
"
"""""""""""""""""""""
lua << EOF
 require('lspkind').init({
     -- enables text annotations
     --
     -- default: true
     with_text = true,

     -- default symbol map
     -- can be either 'default' or
     -- 'codicons' for codicon preset (requires vscode-codicons font installed)
     --
     -- default: 'default'
     preset = 'codicons',

     -- override preset symbols
     --
     -- default: {}
     symbol_map = {
       Text = '',
       Method = 'ƒ',
       Function = '',
       Constructor = '',
       Variable = '',
       Class = '',
       Interface = 'ﰮ',
       Module = '',
       Property = '',
       Unit = '',
       Value = '',
       Enum = '了',
       Keyword = '',
       Snippet = '﬌',
       Color = '',
       File = '',
       Folder = '',
       EnumMember = '',
       Constant = '',
       Struct = ''
     },
 })
EOF
" let g:vista_log_file = './vista.log'
""" Tagbar
nmap <leader>t :TagbarToggle<CR>

""" gitsigns """
lua require('gitsigns').setup()
""" Kommentary """
lua require('kommentary.config').use_extended_mappings()

""" Twilight
lua << EOF
  require("twilight").setup {
    -- your configuration comes here
    -- or leave it empty to use the default settings
    -- refer to the configuration section below
  }
EOF
autocmd VimEnter * TwilightEnable


""" GitLinker """
lua require"gitlinker".setup()



""" telescope setup """
lua << EOF
require('telescope').setup{
        defaults = {
                file_ignore_patterns = { "output/.*", },
        }
}
EOF

"""
"
"    COC
"
"""
let g:python3_host_prog = '/bin/python3'
" Give more space for displaying messages.
set cmdheight=2

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
if has("nvim-0.5.0") || has("patch-8.1.1564")
  " Recently vim can merge signcolumn and number column into one
  set signcolumn=number
else
  set signcolumn=yes
endif

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

" Make <CR> auto-select the first completion item and notify coc.nvim to
" format on enter, <cr> could be remapped by other vim plugin
inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

" Use `[g` and `]g` to navigate diagnostics
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  elseif (coc#rpc#ready())
    call CocActionAsync('doHover')
  else
    execute '!' . &keywordprg . " " . expand('<cword>')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" Formatting selected code.
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder.
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current buffer.
nmap <leader>ac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

" Map function and class text objects
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
omap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap af <Plug>(coc-funcobj-a)
xmap ic <Plug>(coc-classobj-i)
omap ic <Plug>(coc-classobj-i)
xmap ac <Plug>(coc-classobj-a)
omap ac <Plug>(coc-classobj-a)

" Remap <C-f> and <C-b> for scroll float windows/popups.
if has('nvim-0.4.0') || has('patch-8.2.0750')
  nnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  nnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
  inoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<Right>"
  inoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<Left>"
  vnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  vnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
endif

" Use CTRL-S for selections ranges.
" Requires 'textDocument/selectionRange' support of language server.
nmap <silent> <C-s> <Plug>(coc-range-select)
xmap <silent> <C-s> <Plug>(coc-range-select)

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocAction('format')

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Add (Neo)Vim's native statusline support.
" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline.
" set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Mappings for CoCList
" Show all diagnostics.
nnoremap <silent><nowait> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions.
nnoremap <silent><nowait> <space>e  :<C-u>CocList extensions<cr>
" Show commands.
nnoremap <silent><nowait> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document.
nnoremap <silent><nowait> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols.
nnoremap <silent><nowait> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent><nowait> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent><nowait> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list.
nnoremap <silent><nowait> <space>p  :<C-u>CocListResume<CR>

command! -nargs=0 Prettier :CocCommand prettier.formatFile
